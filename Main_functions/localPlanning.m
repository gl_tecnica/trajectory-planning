function [dev_angle, dev_distance, triangle_matrix, car_position_local, carOrientation_local, cum_curvature_t_0] = localPlanning(time, coneData, triangle_matrix, curvature_t_m1, optional_plot)
%
% 1. Generate cartesian map
% 2. Plan path
% 2.1 Advanced Case
% 2.1.1 Delaunay triangulation
% 2.1.2 Create branches
% 2.1.3 Create trajectory
% 2.2 Base Case
% 2.2.1 Create trajectory with offset
% 2.3 Worst Base Case
% 2.4 Freestyle
%
%__________________________________________________________________________
% Input:
%
%       time              = time of current data input
%       coneData          = [ID, distance, angle, color]
%                            ID       = cone ID number
%                            distance = cone distance to car FRONT
%                            angle    = cone angle to car (FRONT) in RAD: 
%                                       -180� to the left, +180� to the 
%                                       right
%                            color    = cone color (blue = 1/yellow = 2)
%       triangle_matrix   = nx3 matrix containing cone IDs of triangles
%
% Output:
%
%       dev_angle         = car angle to trajectory (car wants to steer to
%                           the right: pos. value)
%       dev_distance      = car distance to trajectory (car on the right:
%                           pos. value)
%       triangle_matrix   = nx3 matrix containing cone IDs of triangles
%
%__________________________________________________________________________
% Example:
%
% time     = 1;
% ID       = 1:1:11;
% distance = [3.75 3.75 6.408 6.408 10.738 10.738 15.02 15.89 19.8573 17.8273 22.562];
% angle    = [-0.6435 0.6435 -0.35877 0.35877 -0.21109 0.21109 0.05 0.3367 0.19 0.3883 0.3743];
% color    = [2 1 2 1 2 1 2 1 2 1 2];
% coneData = [ID' distance' angle' color'];
% triangle_matrix = [];
% optional_plot   = 1;
% [dev_angle, dev_distance, triangle_matrix] = localPlanning(time, coneData, triangle_matrix, optional_plot);
% 
%__________________________________________________________________________
%__________________________________________________________________________

frozen_ring_radius     = 1;



if nargin < 5
    optional_plot = false;
end
advanced_case = 0; % for plot

if size(coneData,1) >= 1
    number_of_blue_cones   = size(find(coneData(:,4)==1),1);
    number_of_yellow_cones = size(find(coneData(:,4)==2),1);

    % Check if cones of other color are too far way/on another lane -> Base case
    counter_blue = 0;
    counter_yellow = 0;
    limit_cones_of_one_number = 2;
    coneData_sorted = sortrows(coneData, 2);
    if size(coneData_sorted,1) > 6
        for i=1:1:size(coneData_sorted,1)-1
            if coneData_sorted(i,4) == 1
                counter_blue = counter_blue + 1;
                counter_yellow = 0;
                if counter_blue > limit_cones_of_one_number && coneData_sorted(i+1,4) == 2
                    coneData_sorted(i+1:end) = [];
                    coneData = coneData_sorted;
                    number_of_blue_cones   = size(find(coneData(:,4)==1),1);
                    number_of_yellow_cones = size(find(coneData(:,4)==2),1);
                    break;
                end
            else
                counter_yellow = counter_yellow + 1;
                counter_blue = 0;
                if counter_yellow > limit_cones_of_one_number && coneData_sorted(i+1,4) == 1
                    coneData_sorted(i+1:end,:) = [];
                    coneData = coneData_sorted;
                    number_of_blue_cones   = size(find(coneData(:,4)==1),1);
                    number_of_yellow_cones = size(find(coneData(:,4)==2),1);
                    break;
                end
            end
        end
    end
end
%% 1. Generate cartesian cone map
if size(coneData,1) >= 1
    coneDataCartesian = [];
    for i = 1:length(coneData(:,1))
        coneDataCartesian = [coneDataCartesian; coneData(i,1) sin(coneData(i,3))*coneData(i,2) cos(coneData(i,3))*coneData(i,2) coneData(i,4)];
    end
end

%% 2. Plan path ***********************************************************
%  ************************************************************************
%% 2.1 Advanced case
if (number_of_blue_cones >= 2 && number_of_yellow_cones >= 1) || (number_of_yellow_cones >= 2 && number_of_blue_cones >= 1)
    advanced_case = 1; % for plot
    % 2.1.1 Delaunay triangulation
    delaunay_triangles = delaunayTriangulation(coneDataCartesian(:,2),coneDataCartesian(:,3));
    triangle_matrix = delaunay_triangles.ConnectivityList;

    % 2.2.2 Create branches
    triangle_branches = {};
    counter = 1;
    for i=1:1:length(triangle_matrix(:,1)) % through all triangles
        triangle_branches(i).coordinates = [];
        triangle_branches(i).cone_colors   = [];
        for j=1:1:3 % through all lines of one triangle
            middle_point_x = ((coneDataCartesian(triangle_matrix(i,j),2)-...
                coneDataCartesian(triangle_matrix(i,mod(j,3)+1),2))/2)+coneDataCartesian(triangle_matrix(i,mod(j,3)+1),2);
            middle_point_y = ((coneDataCartesian(triangle_matrix(i,j),3)-...
                coneDataCartesian(triangle_matrix(i,mod(j,3)+1),3))/2)+coneDataCartesian(triangle_matrix(i,mod(j,3)+1),3);
            triangle_branches(i).coordinates(end+1,:) = [middle_point_x middle_point_y];
            triangle_branches(i).cone_colors(end+1) = str2double(strcat(num2str(coneDataCartesian(triangle_matrix(i,j),4)), num2str(coneDataCartesian(triangle_matrix(i,mod(j,3)+1),4)))); 
            counter = counter + 1;
        end
    end

    % 2.2.3 Create trajectory
    % Cut irrelevant branches
    trajectory = [];
    for i=1:1:length(triangle_branches(1,:))
        for j=1:1:3
            if triangle_branches(i).cone_colors(j) == 21 || triangle_branches(i).cone_colors(j) == 12
                trajectory = [trajectory; triangle_branches(i).coordinates(j,:)];
            end
        end
    end
    % Order trajectory points - closest point is assumed to be first one
    distance_to_trajectory_points = sqrt(trajectory(:,1).^2+trajectory(:,2).^2);
    [~,index_start_trajectory] = min(distance_to_trajectory_points);
    trajectory_copy = trajectory;
    % If starting point is on 2nd position, flip matrix and then delete
    % already covered points
    if mod(index_start_trajectory,2) == 0
        trajectory_ordered(1:2,:) =  trajectory(index_start_trajectory-1:index_start_trajectory,:);
        trajectory_ordered = flip(trajectory_ordered);
        trajectory_copy(index_start_trajectory-1:index_start_trajectory,:) = [];
    else
        trajectory_ordered(1:2,:) =  trajectory(index_start_trajectory:index_start_trajectory+1,:);
        trajectory_copy(index_start_trajectory:index_start_trajectory+1,:) = []; 
    end
    
    % Order rest, last point is first point of next segment
    while ~isempty(trajectory_copy)
        for i=1:1:length(trajectory_copy)
            if sum(abs(trajectory_copy(i,:)-trajectory_ordered(end,:))<0.001)>1 || sum(abs(trajectory_copy(i,:)-trajectory_ordered(end-1,:))<0.001)>1
                index_trajectory = i;
                break;
            end
        end
        if mod(index_trajectory,2) == 1
            trajectory_ordered(end+1:end+2,:) =  trajectory_copy(index_trajectory:index_trajectory+1,:);
            trajectory_copy(index_trajectory:index_trajectory+1,:) = [];
        else
            trajectory_ordered(end+1,:) =  trajectory_copy(index_trajectory,:);
            trajectory_ordered(end+1,:) =  trajectory_copy(index_trajectory-1,:);
            trajectory_copy(index_trajectory-1:index_trajectory,:) = [];
        end
    end
    % Cut double values in ordered trajectory
    trajectory_ordered = round(trajectory_ordered, 2); % Round due to numerical noise
    trajectory_ordered_cut = unique(trajectory_ordered(:,:), 'rows', 'stable');
    trajectory_ordered_cut(abs(trajectory_ordered_cut(:,1))<0.001) = 0;
    
    % Add first part of trajectory from car position to first trajectory coordinate
    trajectory_ordered_cut = [0 0; trajectory_ordered_cut];
    % Make trajectory tangent continous
    angleDiffLimit = 5; % in degrees
    trajectory_tangentContinous = trajectoryTangentContinuous(trajectory_ordered_cut, angleDiffLimit);
    
    
%% 2.2 Base Case
elseif (number_of_blue_cones >= 2 && number_of_yellow_cones == 0) || (number_of_yellow_cones >= 2 && number_of_blue_cones == 0)
    % 2.2.1 Create trajectory with offset
    coneDataCartesian_cut = [];
    for i=1:1:size(coneData,1)
        if coneData(i,2) < 6 && size(find(coneData(:,2)<6),1)>2
            coneDataCartesian_cut(end+1,:) = coneDataCartesian(i,:);
        end
    end
    if isempty(coneDataCartesian_cut)
        coneDataCartesian_cut = coneDataCartesian;
    end
    coneDataCartesian_cut = sortrows(coneDataCartesian_cut, 3);
    trajectory_ordered_cut = coneDataCartesian_cut(:,2:3);
    % Move trajectory paralelly towards middle of track
    d = 2;                                                                          %!!!!!!!!!!!!!!!!!!
    if coneDataCartesian(1,4) == 1
        d = d * (-1);
    end
    [x_inner, y_inner] = parallel_curve_localPlanning(trajectory_ordered_cut(:,1), trajectory_ordered_cut(:,2), d, 0, 0);
    trajectory_parallel = [x_inner y_inner];
    % Make trajectory tangent continous
    trajectory_parallel_origin = [0 0; trajectory_parallel];
    angleDiffLimit = 5; % in degrees
    trajectory_tangentContinous = trajectoryTangentContinuous(trajectory_parallel_origin, angleDiffLimit);
    % Maybe use different parallel function
    
%% 2.3 Worst Base Case
% elseif number_of_blue_cones == 1 && number_of_yellow_cones == 0 || number_of_yellow_cones == 1 && number_of_blue_cones == 0

%% 2.4 Freestyle
else %if isempty(coneData(:,1))
    
    curvature_factor = 0.2;
    if curvature_t_m1 ~= 0
        radius_freestyle_trajectory = 1/(curvature_factor*abs(curvature_t_m1));
        theta = 0:pi/50:pi/2;
        xunit = radius_freestyle_trajectory * cos(theta) - radius_freestyle_trajectory;
        yunit = radius_freestyle_trajectory * sin(theta);
        if curvature_t_m1 < 0
            xunit = xunit * (-1);
        end
        trajectory_tangentContinous = [xunit' yunit'];
    else
        trajectory_tangentContinous = [0 0; 0 10];
    end
    
end

%% Calculate frozen ring **************************************************
%  ************************************************************************

frozen_ring_angle_step = linspace(0, pi, 100);
frozen_ring_X          = cos(frozen_ring_angle_step)*frozen_ring_radius;
frozen_ring_Y          = sin(frozen_ring_angle_step)*frozen_ring_radius;
frozen_ring            = [frozen_ring_X' frozen_ring_Y'];
frozen_ring            = [frozen_ring; frozen_ring(1,:)];

% Intersection with trajectory
if sqrt(trajectory_tangentContinous(end,1)^2+trajectory_tangentContinous(end,2)^2)<frozen_ring_radius
    delta_x = trajectory_tangentContinous(end,1)-trajectory_tangentContinous(end-1,1);
    delta_y = trajectory_tangentContinous(end,2)-trajectory_tangentContinous(end-1,2);
    trajectory_tangentContinous(end+1,:) = bsxfun(@plus, trajectory_tangentContinous(end,:), [50*delta_x 50*delta_y]);
end
warning('off','all')
[frozen_ring_intersection_point_x, frozen_ring_intersection_point_y] = intersections(frozen_ring(:,1), frozen_ring(:,2), trajectory_tangentContinous(:,1), trajectory_tangentContinous(:,2));
warning('on','all')
frozen_ring_intersection_point = [frozen_ring_intersection_point_x frozen_ring_intersection_point_y];
% Throw away non zeros
frozen_ring_intersection_point = frozen_ring_intersection_point(frozen_ring_intersection_point(:,2)>0.1 | frozen_ring_intersection_point(:,2)<-0.1, :);
% Following assumption might be wrong                                                    !!!!!!!!!!!!!!!!!!!
if size(frozen_ring_intersection_point,1) > 1
    [~,index] = min(frozen_ring_intersection_point(:,1));
    frozen_ring_intersection_point = frozen_ring_intersection_point(index(1), :);
end
car_position_local = frozen_ring_intersection_point;

% Calculate new orientation (FOR MATLAB SIMULATION)
for i=1:1:size(trajectory_tangentContinous, 1)
    length_trajectory = sqrt(trajectory_tangentContinous(i,1)^2+trajectory_tangentContinous(i,2)^2);
    if length_trajectory > frozen_ring_radius
        index = i;
        break;
    end
end
trajectory_to_frozen_point = [trajectory_tangentContinous(1:index-1,:); frozen_ring_intersection_point];
carOrientation_local = abs(acos(trajectory_to_frozen_point(end,2)/norm(trajectory_to_frozen_point(end,:))));
if carOrientation_local > pi/4
    carOrientation_local = pi/2 - carOrientation_local;
end
if trajectory_to_frozen_point(end,1) < 0 
    carOrientation_local = carOrientation_local * -1;
end

%% Calculate cumulative curvature until intersection point with frozen ring
% Interpolate
ds = 1;
trajectory_to_frozen_point = unique(trajectory_to_frozen_point, 'rows', 'stable');
trajectory_to_frozen_point_Ynew = 0:ds/trajectory_to_frozen_point(end,2):trajectory_to_frozen_point(end,2);
try
    trajectory_to_frozen_point_Xnew = interp1(trajectory_to_frozen_point(:,2)', trajectory_to_frozen_point(:,1)', trajectory_to_frozen_point_Ynew);
    dx = diff([0 trajectory_to_frozen_point_Xnew])/ds;
    dy = diff([0 trajectory_to_frozen_point_Ynew])/ds;
    ddx = diff([0 dx])/ds;
    ddy = diff([0 dy])/ds;
    for i = 1:1:size(dx,2)
        curvature_t_0(i) = ((dx(i)*ddy(i))-(ddx(i)*dy(i)))/((dx(i)^2+dy(i)^2)^1.5);
        if isnan(curvature_t_0(i))
            curvature_t_0(i) = 0;
        end
    end
    cum_curvature_t_0 = sum(curvature_t_0);
catch
    cum_curvature_t_0 = 0;
end

%% PLOT *******************************************************************
%       *******************************************************************

if optional_plot && advanced_case
    fighandles = findall(allchild(0), 'type', 'figure');
    if ~isempty(fighandles)
        fig = fighandles(1);
        allaxes = findall(fig, 'type', 'axes');
        delete(allaxes);
    end
    axes('Position',[.75 .25 .15 .5]);
    box on
    % Cone Data
    for i=1:1:length(coneDataCartesian(:,1))
        if coneDataCartesian(i,4) == 1
            colorPlot = 'bo';
        else
            colorPlot = 'yo';
        end
        plot(coneDataCartesian(i,2), coneDataCartesian(i,3), colorPlot);
        hold on;
    end
    % Car
    rectangle('Position',[-0.6 -3.2 1.2 3.2], 'EdgeColor', 'r')
    % Triangles
    triplot(delaunay_triangles, 'k');
    % Branches
    for i=1:1:length(triangle_branches(1,:))
        for j=1:1:3
            x_branch = [triangle_branches(i).coordinates(j,1) triangle_branches(i).coordinates(mod(j,3)+1,1)];
            y_branch = [triangle_branches(i).coordinates(j,2) triangle_branches(i).coordinates(mod(j,3)+1,2)];
            plot(x_branch, y_branch, 'g--');
        end
    end
    % Trajectory
    plot(trajectory_tangentContinous(:,1), trajectory_tangentContinous(:,2), 'r-');
    % Frozen ring
    plot(frozen_ring(:,1), frozen_ring(:,2), 'b-');
    
    axis equal;
    xlim([-8,8]);
    grid on;
end

dev_angle = 0;
dev_distance = 0;

end

