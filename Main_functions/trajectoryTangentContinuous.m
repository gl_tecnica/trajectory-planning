function trajectory_tangentContinous = trajectoryTangentContinuous(trajectory, angleDiffLimit)

% Input:
%
%     trajectory      = x,y matrix
%     angleDiffLimit  = allowed angle difference in degrees  
%
% Output:
%
%     trajectory_tangentContinous = tangentcontinous trajectory
%
%

%%
% degree to rad
angleDiffLimit = angleDiffLimit*pi/180;
% Calculate slope of trajectory segments
trajectory_slope = atan(diff(trajectory(:,2))./diff(trajectory(:,1)));
% Define default line for radi
vertical_line_default_x = [-20 20];
vertical_line_default_y = [0 0];
vertical_line_default = [vertical_line_default_x; vertical_line_default_y];
% Clothoid
clothoid_use = false;
% Optional plot
optional_plot = false;

% Calculate arcs
trajectory_tangentContinous = trajectory(1,:);
for i=1:1:length(trajectory_slope)-1
    if abs(trajectory_slope(i+1)-trajectory_slope(i)) > angleDiffLimit
        %% Calculate orthogonal lines
        % Vertical Line to 1st middle point
        middle_point_line1 = (bsxfun(@plus, trajectory(i+1,:), trajectory(i,:)))/2;
        transformation_angle = -trajectory_slope(i) + pi/2;
        transformation_matrix_line1 = [cos(transformation_angle) sin(transformation_angle); -sin(transformation_angle) cos(transformation_angle)];
        vertical_line1 = transformation_matrix_line1*vertical_line_default;
        vertical_line1 = vertical_line1';
        vertical_line1_moved =  bsxfun(@plus, vertical_line1, middle_point_line1);
        % Vertical Line to 2nd middle point
        middle_point_line2 = (bsxfun(@plus, trajectory(i+2,:), trajectory(i+1,:)))/2;
        transformation_angle = -trajectory_slope(i+1) + pi/2;
        transformation_matrix_line2 = [cos(transformation_angle) sin(transformation_angle); -sin(transformation_angle) cos(transformation_angle)];
        vertical_line2 = transformation_matrix_line2*vertical_line_default;
        vertical_line2 = vertical_line2';
        vertical_line2_moved =  bsxfun(@plus, vertical_line2, middle_point_line2);
        % Optional plot of center lines
        if optional_plot
            close;
            plot(vertical_line1_moved(:,1), vertical_line1_moved(:,2), 'k--');
            hold on;
            axis equal;
            grid on;
            plot(vertical_line2_moved(:,1), vertical_line2_moved(:,2), 'k--');
            plot(trajectory(:,1), trajectory(:,2), 'r-');
        end

        %% Move vertical Line from longer trajectory segment
        vector_to_middle_point_line1 = trajectory(i+1,:) - middle_point_line1;
        vector_to_middle_point_line2 = trajectory(i+2,:) - middle_point_line2;
        if norm(vector_to_middle_point_line1) < norm(vector_to_middle_point_line2)
            vertical_line_keep = vertical_line1_moved;
            middle_point_keep = middle_point_line1;
            vector_to_replacement = vector_to_middle_point_line2/norm(vector_to_middle_point_line2)*norm(vector_to_middle_point_line1);
            replacement_point = trajectory(i+1,:) + vector_to_replacement;
            movement_vector = replacement_point - middle_point_line2;
            vector_line_replaced = bsxfun(@plus, vertical_line2_moved, movement_vector);
            middle_point_replaced = bsxfun(@plus, middle_point_line2, movement_vector);
            use_replaced_point_for_final_movement = 0;
        else
            vertical_line_keep = vertical_line2_moved;
            middle_point_keep = middle_point_line2;
            vector_to_replacement = vector_to_middle_point_line1/norm(vector_to_middle_point_line1)*norm(vector_to_middle_point_line2);
            replacement_point = trajectory(i,:) + vector_to_replacement;
            movement_vector = replacement_point - middle_point_line1;
            vector_line_replaced = bsxfun(@minus, vertical_line1_moved, movement_vector);
            middle_point_replaced = bsxfun(@minus, middle_point_line1, movement_vector);
            use_replaced_point_for_final_movement = 1;
        end
        if optional_plot
            plot(vector_line_replaced(:,1), vector_line_replaced(:,2), 'r--');
        end
        
        %% Find intersection
        [intersection_point_x, intersection_point_y] = intersections(vertical_line_keep(:,1),vertical_line_keep(:,2), vector_line_replaced(:,1),vector_line_replaced(:,2));
        intersection_point = [intersection_point_x intersection_point_y];
        stop_loop_one_time = false;
        if isempty(intersection_point)
            stop_loop_one_time = true;
            connector_Final = trajectory(i:i+1,:);
        end
        if ~stop_loop_one_time
            % Find radius and calculate angle between vertical lines
            radius_line = norm(bsxfun(@minus, middle_point_keep(1,:), intersection_point));
            radius_1_vector = -bsxfun(@minus, intersection_point, middle_point_keep(1,:));
            radius_2_vector = -bsxfun(@minus, intersection_point, middle_point_replaced(1,:));
            phi_clothoid = acos((radius_1_vector*radius_2_vector')/(norm(radius_1_vector)*norm(radius_2_vector)));

            %% Clothoid
            if clothoid_use
                % Create clothoid curve
                clothoid_step_size = 0.1;
                clothoid_arcRadius = radius_line;
                clothoid_length = phi_clothoid/2 * clothoid_arcRadius;
                [x_clothoid, y_clothoid,~] = clothoid_loc_2(clothoid_step_size, clothoid_arcRadius, clothoid_length);
                if trajectory_slope(i+1) < trajectory_slope(i)
                    y_clothoid = -y_clothoid;
                end
                clothoid_matrix = [x_clothoid; y_clothoid];
                transformation_angle_clothoid = -trajectory_slope(i);
                transformation_matrix_line1_clothoid = [cos(transformation_angle_clothoid) sin(transformation_angle_clothoid); -sin(transformation_angle_clothoid) cos(transformation_angle_clothoid)];
                clothoid_matrix = transformation_matrix_line1_clothoid*clothoid_matrix;
                clothoid_matrix = clothoid_matrix';

                % Mirror clothoid
                clothoid_matrix_mirrored = [clothoid_matrix(:,1)'; -clothoid_matrix(:,2)'];
                clothoid_matrix_mirrored = clothoid_matrix_mirrored - clothoid_matrix_mirrored(:,end);
                transformation_angle_clothoid = phi_clothoid;
                transformation_matrix_line2_clothoid = [cos(transformation_angle_clothoid) sin(transformation_angle_clothoid); -sin(transformation_angle_clothoid) cos(transformation_angle_clothoid)];
                clothoid_matrix_mirrored = transformation_matrix_line2_clothoid*clothoid_matrix_mirrored;
                clothoid_matrix_mirrored = clothoid_matrix_mirrored';
                clothoid_matrix_mirrored = clothoid_matrix_mirrored + clothoid_matrix(end,:);

                % Complete clothoid
                clothoid_matrix_mirrored_flipped = flip(clothoid_matrix_mirrored);
                clothoid_total = [clothoid_matrix; clothoid_matrix_mirrored_flipped(2:end,:)];

                % Move clothoid
                connector_Final = clothoid_total + vertical_line_keep(1,:);
            else % use arc
                arc_connector_angle = linspace(0, phi_clothoid, 100);
                arc_connector_x = cos(arc_connector_angle) * radius_line;
                arc_connector_y = sin(arc_connector_angle) * radius_line;
                arc_connector = [arc_connector_x' arc_connector_y'];
                connector_Final = bsxfun(@minus, arc_connector, arc_connector(1,:)); 
                % Mirror circle segment if neccessary
                if abs(trajectory_slope(i))-abs(trajectory_slope(i+1)) > 0 && trajectory_slope(i+1) > 0 || ...
                        abs(trajectory_slope(i))-abs(trajectory_slope(i+1)) < 0 && trajectory_slope(i+1) < 0
                    connector_Final(:,1) = connector_Final(:,1) * -1;
                end
                % Rotate
                if trajectory_slope(i)>0
                    transformation_angle_arc = -trajectory_slope(i)+pi/2;
                else
                    transformation_angle_arc = -trajectory_slope(i)-pi/2;
                end
                transformation_matrix_arc = [cos(transformation_angle_arc) sin(transformation_angle_arc); -sin(transformation_angle_arc) cos(transformation_angle_arc)];
                connector_Final = transformation_matrix_arc*connector_Final';
                connector_Final = connector_Final';
                % Move to last segment
                if use_replaced_point_for_final_movement == 1;
                    connector_Final = bsxfun(@plus, connector_Final, middle_point_replaced(1,:));
                else
                    connector_Final = bsxfun(@plus, connector_Final, middle_point_keep(1,:));
                end
            end
        end
        %% Merge trajectories
        if isempty(trajectory_tangentContinous)
            trajectory_tangentContinous = trajectory(1:i,:);
        end
        if exist('connector_Final', 'var')
            trajectory_tangentContinous = [trajectory_tangentContinous; connector_Final];
        end
        if optional_plot
            hold on;
            plot(trajectory(:,1), trajectory(:,2), 'k--');
            axis equal;
%             plot(connector_Final(:,1), connector_Final(:,2), 'k-');
            plot(trajectory_tangentContinous(:,1), trajectory_tangentContinous(:,2), 'm-');
        end
    end
end
trajectory_tangentContinous = [trajectory_tangentContinous; trajectory(i+2:end,:)];

end

