function cameraData = createCameraView(cameraData, centerPosition)

%% Unpack structs
camera1_range            = cameraData.camera1_range;
camera1_apertureAngle    = cameraData.camera1_apertureAngle;
camera2_range            = cameraData.camera2_range;
camera2_apertureAngle    = cameraData.camera2_apertureAngle;
distance_between_cameras = cameraData.distance_between_cameras;

%% Define camera view, car starts always towards right side
%% Camera 1
camera1_apertureAngle_rad = camera1_apertureAngle/180*pi;
camera1_angle_steps = linspace(-camera1_apertureAngle_rad/2,camera1_apertureAngle_rad/2,100);
camera1_arcSegment_X = camera1_range * cos(camera1_angle_steps);
camera1_arcSegment_Y = camera1_range * sin(camera1_angle_steps);
camera1_arcSegment = [camera1_arcSegment_X' camera1_arcSegment_Y'];
camera1_arcSegment = bsxfun(@plus, camera1_arcSegment, centerPosition);
camera1_view = [centerPosition; camera1_arcSegment; centerPosition];
camera1_view(:,1) = camera1_view(:,1) - distance_between_cameras;
        
%% Camera 2
camera2_apertureAngle_rad = camera2_apertureAngle/180*pi;
camera2_angle_steps = linspace(-camera2_apertureAngle_rad/2, camera2_apertureAngle_rad/2, 100);
camera2_arcSegment_X = camera2_range * cos(camera2_angle_steps);
camera2_arcSegment_Y = camera2_range * sin(camera2_angle_steps);
camera2_arcSegment = [camera2_arcSegment_X' camera2_arcSegment_Y'];
camera2_arcSegment = bsxfun(@plus, camera2_arcSegment, centerPosition);
camera2_view = [centerPosition; camera2_arcSegment; centerPosition];

%% Save in structs
cameraData.camera1_view         = camera1_view;
cameraData.camera2_view         = camera2_view;

end

