function conesWithinRangeData = getConeDataFromRaceTrack(time, cameraData, carData, conePosition_blue, conePosition_yellow)

% %% Example
% carPosition = [0 0]; % always measured from car front
% carOrientation = 0; % in degree, count counter-clockwise, start with horizontal right

%% Unpack structs
camera1_view = cameraData.camera1_view_new;
camera2_view = cameraData.camera2_view_new;
car_coordinate_system_X = carData.car_coordinate_system_X_new;
carPosition = carData.carPosition;

%% Check which cones are within range
conesWithinRangeData = [];
% Blue cones
for i = 1:1:size(conePosition_blue, 1)
    if inpolygon(conePosition_blue(i,1), conePosition_blue(i,2), camera1_view(:,1), camera1_view(:,2)) || ...
            inpolygon(conePosition_blue(i,1), conePosition_blue(i,2), camera2_view(:,1), camera2_view(:,2))
        % Calculate distance to car FRONT
        distance_to_cone = sqrt((carPosition(1,1)-conePosition_blue(i,1))^2+(carPosition(1,2)-conePosition_blue(i,2))^2);
        % Calculate angle to car FRONT: -180� to the left, +180� to the right
        vector_to_cone = bsxfun(@minus, conePosition_blue(i,:), carPosition);
        vector_car_X_coordinate = bsxfun(@minus, car_coordinate_system_X(2,:), car_coordinate_system_X(1,:));
        angle_to_cone = acos((vector_car_X_coordinate*vector_to_cone')/(norm(vector_car_X_coordinate)*norm(vector_to_cone)));
        det_for_angle_orientation = det([vector_car_X_coordinate' vector_to_cone']);
        if det_for_angle_orientation > 0
            angle_to_cone = angle_to_cone*-1;
        end
        % ID !!!
        conesWithinRangeData(end+1,:) = [time, distance_to_cone, angle_to_cone, 1];
    end
end
% Yellow cones
for i = 1:1:size(conePosition_yellow, 1)
    if inpolygon(conePosition_yellow(i,1), conePosition_yellow(i,2), camera1_view(:,1), camera1_view(:,2)) || ...
            inpolygon(conePosition_yellow(i,1), conePosition_yellow(i,2), camera2_view(:,1), camera2_view(:,2))
        % Calculate distance to car FRONT
        distance_to_cone = sqrt((carPosition(1,1)-conePosition_yellow(i,1))^2+(carPosition(1,2)-conePosition_yellow(i,2))^2);
        % Calculate angle to car FRONT: -180� to the left, +180� to the right
        vector_to_cone = bsxfun(@minus, conePosition_yellow(i,:), carPosition);
        vector_car_X_coordinate = bsxfun(@minus, car_coordinate_system_X(2,:), car_coordinate_system_X(1,:));
        angle_to_cone = acos((vector_car_X_coordinate*vector_to_cone')/(norm(vector_car_X_coordinate)*norm(vector_to_cone)));
        det_for_angle_orientation = det([vector_car_X_coordinate' vector_to_cone']);
        if det_for_angle_orientation > 0
            angle_to_cone = angle_to_cone*-1;
        end
        % ID !!!
        conesWithinRangeData(end+1,:) = [time, distance_to_cone, angle_to_cone, 2];
    end
end

end

