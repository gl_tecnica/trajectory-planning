function [cameraData, carData] = moveCar(cameraData, carData, carPosition_new_local, carOrientation_new_local)

%% Unpack structs
camera1_view                 = cameraData.camera1_view;
camera2_view                 = cameraData.camera2_view;
car_length                   = carData.car_length;
carPosition_old              = carData.carPosition;
carOrientation_old           = carData.carOrientation;
car_coordinates_old          = carData.car_coordinates;
car_coordinate_system_X_old  = carData.car_coordinate_system_X;
car_coordinate_system_Y_old  = carData.car_coordinate_system_Y;

carOrientation_new_global = carOrientation_old + carOrientation_new_local;

%% Transfer car from local to global system
localToGlobal_angle = pi/2 + carOrientation_old; % because angle is negative to the left
localToGlobal_transformation_matrix = [cos(localToGlobal_angle) sin(localToGlobal_angle); -sin(localToGlobal_angle) cos(localToGlobal_angle)];
carPosition_new_global_rotate = localToGlobal_transformation_matrix*carPosition_new_local';
carPosition_new_global_rotate = carPosition_new_global_rotate';
carPosition_new_global = bsxfun(@plus, carPosition_old, carPosition_new_global_rotate);

%% Transfer car and coordinate system data according to car position and orientation
carOrientation_transformation_matrix = [cos(carOrientation_new_global) sin(carOrientation_new_global);...
    -sin(carOrientation_new_global) cos(carOrientation_new_global)];

% Translate car to center coordinates and rotate
car_coordinates_centered = bsxfun(@plus, car_coordinates_old, car_length);
car_rotation = carOrientation_transformation_matrix*car_coordinates_centered';
car_rotation = car_rotation';
car_coordinates_new = bsxfun(@minus, car_rotation, car_rotation(1,:));
car_coordinates_new = bsxfun(@plus, car_coordinates_new, carPosition_new_global);

% Car coordination system
car_coordinate_rotation_X_new = carOrientation_transformation_matrix*car_coordinate_system_X_old';
car_coordinate_rotation_X_new = car_coordinate_rotation_X_new';
car_coordinate_system_X_new =  bsxfun(@plus, car_coordinate_rotation_X_new, carPosition_new_global);

car_coordinate_rotation_Y_new = carOrientation_transformation_matrix*car_coordinate_system_Y_old';
car_coordinate_rotation_Y_new = car_coordinate_rotation_Y_new';
car_coordinate_system_Y_new =  bsxfun(@plus, car_coordinate_rotation_Y_new, carPosition_new_global);

%% Define camera view, car starts always towards right side
%% Camera 1 - Bosch
% camera1_view = bsxfun(@plus, camera1_view, [distance_between_cameras 0]);
% Rotate according to car orientation
camera1_view_rotation = carOrientation_transformation_matrix*camera1_view';
camera1_view_rotation = camera1_view_rotation';
camera1_view_new =  bsxfun(@plus, camera1_view_rotation, carPosition_new_global);
        
%% Camera 2
% Rotate according to car orientation
camera2_view_rotation = carOrientation_transformation_matrix*camera2_view';
camera2_view_rotation = camera2_view_rotation';
camera2_view_new =  bsxfun(@plus, camera2_view_rotation, carPosition_new_global);

%% Save in structs
cameraData.camera1_view_new         = camera1_view_new;
cameraData.camera2_view_new         = camera2_view_new;
carData.carPosition                 = carPosition_new_global;
carData.carOrientation              = carOrientation_new_global;
carData.car_coordinates_new         = car_coordinates_new;
carData.car_coordinate_system_X_new = car_coordinate_system_X_new;
carData.car_coordinate_system_Y_new = car_coordinate_system_Y_new;

end

