Instructions:
1. Download files from Repo 
2. Select Track in MAIN script in line 7
3. Run.

More detailed instructions will be added soon.

For a short demonstration of the code, check out the video on my Youtube channel:
https://www.youtube.com/watch?v=hyihFKrrGTo
 
Plus, I also created a random race track generator which automatically creates 
FSG conform race tracks that can be implemented into this development environment.

For questions to the code or feedback, feel free to text me:
gl_tecnica@gmx.de