%% MAIN: Local Planning simulation

% For questions to the code or feedback, feel free to text me:
% gl_tecnica@gmx.de

%% Load race track
raceTrackPath = [pwd '\Example_Tracks\Track_1.mat']; % choose track 1 - 6
load(raceTrackPath);

%% Definition
simulation_speed = 100000; % Select value between 1 and 100.000 to adapt simulation speed
infinite_test_run = 0;
optional_plot = true;

cameraData = {};
cameraData.camera1_range            = 15;      % Bosch camera
cameraData.camera1_apertureAngle    = 45;      % in degree
cameraData.camera2_range            = 8;       % Intel camera
cameraData.camera2_apertureAngle    = 120;     % in degree
cameraData.distance_between_cameras = 1.5;     % Intel camera in front, both cameras centered in x direction
cameraData.camera1_view             = [];
cameraData.camera2_view             = [];
cameraData.camera2_view_new         = [];

carData = {};
carData.carPosition                 = [0 0];   % always measured from car front
carData.carOrientation              = 0;       % in degree, count counter-clockwise, start with horizontal right
carData.car_length                  = 3.2; 
carData.car_width                   = 1.2;
carData.car_coordinates             = [0 0; 0 -carData.car_width/2; -carData.car_length...
    -carData.car_width/2; -carData.car_length carData.car_width/2; 0 carData.car_width/2; 0 0];
carData.car_coordinates_new         = [];
carData.car_coordinate_system_X     = [0 0; 1 0];
carData.car_coordinate_system_X_new = [0 0; 1 0];
carData.car_coordinate_system_Y     = [0 0; 0 1];
carData.car_coordinate_system_Y_new = [0 0; 0 1];
curvature_t_m1                      = 0;

triangle_matrix = [];
time = 1;

%% Calculate camera views
cameraData = createCameraView(cameraData, carData.carPosition);
cameraData.camera1_view_new = cameraData.camera1_view;
cameraData.camera2_view_new = cameraData.camera2_view;

%% Calculate local trajectory for the whole track
global_trajectory             = carData.carPosition;
start_line_intersection_point = [];
loopCounter = 1;
figure(1)
set(gcf, 'Position', get(0, 'Screensize'));

while isempty(start_line_intersection_point)
    
    %% Get cones within range of cameras
    conesWithinRangeData = getConeDataFromRaceTrack(time, cameraData, carData, roadPointsOuter_cut, roadPointsInner_cut);

    %% Calculate trajectory
    [dev_angle, dev_distance, triangle_matrix, carPosition_local, carOrientation_local, cum_curvature_t_0] = localPlanning(time, conesWithinRangeData, triangle_matrix, curvature_t_m1, optional_plot);
    curvature_t_m1 = cum_curvature_t_0;
        
    %% Move car to new position
    [cameraData, carData] = moveCar(cameraData, carData, carPosition_local, carOrientation_local);
    global_trajectory     = [global_trajectory; carData.carPosition];
    
    %% Plot
    if exist('car_plot', 'var')
        delete(car_plot);delete(trajectory_plot);delete(carCoorX_plot);
        delete(carCoorY_plot);delete(camera1_plot);delete(camera2_plot);
        delete(conesBlue_plot);delete(conesYellow_plot);
    end
    subplot(2,4,[1,2,3,5,6,7]);
    hold on; axis equal; grid on;
    % Plot Car
    car_plot = plot(carData.car_coordinates_new(:,1), carData.car_coordinates_new(:,2), 'r');
    % Plot trajectory
    trajectory_plot  = plot(global_trajectory(:,1), global_trajectory(:,2), 'r');
    % Plot coordinate system
    carCoorX_plot    = plot(carData.car_coordinate_system_X_new(:,1), carData.car_coordinate_system_X_new(:,2), 'k-');
    carCoorY_plot    = plot(carData.car_coordinate_system_Y_new(:,1), carData.car_coordinate_system_Y_new(:,2), 'k-');
    % Plot camera views
    camera1_plot     = plot(cameraData.camera1_view_new(:,1), cameraData.camera1_view_new(:,2), 'b');
    camera2_plot     = plot(cameraData.camera2_view_new(:,1), cameraData.camera2_view_new(:,2), 'g');
    % Plot cones
    conesBlue_plot   = plot(roadPointsOuter_cut(:,1), roadPointsOuter_cut(:,2), 'bo');
    conesYellow_plot = plot(roadPointsInner_cut(:,1), roadPointsInner_cut(:,2), 'yo');
    % Limit axes
    xlim([min(roadPointsOuter_cut(:,1))-10, max(roadPointsOuter_cut(:,1))+10]);
    ylim([min(roadPointsOuter_cut(:,2))-10, max(roadPointsOuter_cut(:,2))+10]);
    
    %% Check if car is at the start again by checking if trajectory intersects start line
    start_line = [0 3;0 -3];
    plot(start_line(:,1), start_line(:,2), 'k-', 'LineWidth', 1);
    [start_line_intersection_point_x, start_line_intersection_point_y] = intersections(global_trajectory(:,1), global_trajectory(:,2), start_line(:,1), start_line(:,2));
    if infinite_test_run == 0
        start_line_intersection_point = [start_line_intersection_point_x start_line_intersection_point_y];
        % Make sure that it doesn't stop at the beginning
        if size(start_line_intersection_point,1) < 2 && start_line_intersection_point(1,1) == 0 && start_line_intersection_point(1,2) == 0 
            start_line_intersection_point = [];
        end
    end
    loopCounter = loopCounter + 1;
    time = time + 1;
    pause(1/simulation_speed);
end



























%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Easter egg
if simulation_speed == 100
    load handel
    sound(y,Fs)
end
